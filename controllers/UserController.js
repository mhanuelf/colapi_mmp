var request = require('request');
var requestJson = require('request-json');
var userController = {};

var usersFile = require('./../users.json');

//configuration data for MLAB
var APIKEY='?apiKey=7uStGNiVeLVP8Yz724d3CErqBBKtdUm1';
var MLAB='https://api.mlab.com/api/1/databases/techuproject/collections/'

//Client declaration to connect with MLAB
var client = requestJson.createClient(MLAB);

//function list for return a user by id

userController.listById= function (req, res) {
  console.log('GET/users/Id');
  var userId=req.params.id;

	client.get('users/'+userId+APIKEY,function(error, response, body){

		res.send(body);
	});
  console.log('id: '+userId);

};


//function list for return userslist
userController.list= function (req, res) {
	var response;
  	console.log('GET/users from MLAB');

	client.get('users'+APIKEY, function(error, response, body) {
		if (error) { return console.log(error); }
		res.send(body);
	});

/*
//get Request using another library

	request(MLAB+users+APIKEY,
		{ json: true },
	 	(err, responseMlab, body) => {
  		if (err) { return console.log(err); }
  		console.log(body.name);
  		console.log(body.explanation);
		console.log(body);
		res.send(body);
	}); */

};


//function create user
userController.createUser = function (req,res){

  console.log('Creacion de usuario exitosa ');
  console.log('Datos body ',req.body.first_name);
  console.log('Datos  header',req.body.last_name);

var user={
	"name":req.body.first_name,
	"lastName": req.body.last_name,
	"email":req.body.email,
	 "country":req.body.country
};

	client.post('users'+APIKEY, user, function(err, response, body) {
		if(err){ return console.log(err); }
		res.send({ "results":"Usuario  creado correctamente"});
	});

/*
	request.post({
	headers: {'content-type' : 'application/x-www-form-urlencoded'},
	url:     MLAB+users+APIKEY,
	body:    user
	}, function(error, response, body){
	console.log(body);
});
  res.send('Usuario '+req.body.first_name+' creado correctamente');

*/
};


//Delete user
userController.deleteUser= function(req,res){
	console.log('DELETE/users/id');
	var userId=req.params.id;
	console.log('User to delete '+userId);
	client.del('users/'+userId+APIKEY, function(err, response, body) {
		if(err){ console.log(err);}
		res.send(body);
	});

};

module.exports = userController;
