var request = require('request');
var requestJson = require('request-json');
var accountController = {};


//configuration data for MLAB
var APIKEY='apiKey=7uStGNiVeLVP8Yz724d3CErqBBKtdUm1';
var MLAB='https://api.mlab.com/api/1/databases/techuproject/collections/accounts'

//Client declaration to connect with MLAB
var client = requestJson.createClient(MLAB);

//function list for return an account by id
accountController.listById= function (req, res) {
  console.log('GET/accounts/Id');
  var accountId=req.params.id;
	client.get('/'+accountId+'?'+APIKEY,function(error, response, body){
		res.send(body);
	});
  console.log('id: '+accountId);

};


//function list for return accounts List by user_id
accountController.list= function (req, res) {
	var response;
  	console.log('GET/accounts from MLAB by user Id');
    var user_id=req.params.id;
    var completeUrl='?q={"user_id":""'+user_id+'""}'+'&'+APIKEY;
    console.log(completeUrl);
	client.get('?q={"user_id":{"$oid":"'+user_id+'"}}'+'&'+APIKEY, function(error, response, body) {
		if (error) { return console.log(error); }
		res.send(body);
	});
};


//function create account
accountController.createAccount = function (req,res){

  console.log('Creacion de cuenta exitosa ');
  console.log('Datos user_id ',req.body.user_id);
  console.log('Datos  iban',req.body.iban);

var account={
	"user_id":req.body.user_id,
	"IBAN": req.body.iban,
	"balance":req.body.balance
};

	client.post(APIKEY, account, function(err, response, body) {
		if(err){ return console.log(err); }
		res.send('Cuenta '+req.body.iban+' creada correctamente');
	});

};


//Delete account
accountController.deleteAccount= function(req,res){
	console.log('DELETE/accounts/id');
	var accountId=req.params.id;
	console.log('Account to delete '+accountId);
	client.del('/'+accountId+APIKEY, function(err, response, body) {
		if(err){ console.log(err);}
		res.send(body);
	});

};

module.exports = accountController;
