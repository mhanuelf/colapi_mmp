var express = require('express');
var router = express.Router();

var user = require("../controllers/UserController.js");
var login = require("../controllers/LoginController.js");
var account = require("../controllers/AccountController.js");

var movement = require("../controllers/MovementController.js");

/*
module.exports = function() {
  return function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With"); next();
  };
};
*/

// Get all users
router.get('/users', user.list);

//Get user by Id
router.get('/users/:id', user.listById);

router.post('/users/', user.createUser);

router.delete('/users/:id',user.deleteUser);


//Login - Logout File
router.post('/loginfile/',login.loginFile);
router.post('/logoutfile/',login.logoutFile);

//Login with MLAB

router.post('/login/',login.validate);

// Get all accounts
router.get('/accounts', account.list);

//Get accounts by user Id
router.get('/users/:id/accounts/', account.list);

//Create account
router.post('/accounts/', account.createAccount);

//Delete account
router.delete('/accounts/:id',account.deleteAccount);

//Get movements by acount and user
router.get('/movements/:id',movement.list);

module.exports = router;
