var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
var morgan = require('morgan');
var APP='/colapi/v1'
var router = express.Router();
// parse application/json

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false }));


var users = require('./routes/users');

//log the requests in the console
app.use(morgan('dev'));

//Module register users routes
app.use(APP, users);

// Passport
var passport = require("passport");
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(userId, done) {
  User.findById(userId, (err, user) => done(err, user));
});

// Passport Local
var LocalStrategy = require("passport-local").Strategy;
var local = new LocalStrategy((username, password, done) => {
  User.findOne({ username })
    .then(user => {
      if (!user || !user.validPassword(password)) {
        done(null, false, { message: "Invalid username/password" });
      } else {
        done(null, user);
      }
    })
    .catch(e => done(e));
});
passport.use("local", local);

app.all('/', function(req, res, next) {
  console.log("validate headers");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
  });

console.log('Node server started on: ' + port);

//Listening port of the aplication
app.listen(port);
