var mocha =require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server.js');


var should= chai.should();

chai.use(chaiHttp); //configurar chai con mòdulo http


describe('Test my TechU app',() => {
  it('My app works ', (done) => {
    chai.request('http://localhost:3000')
    .get('/colapi/v1/users')
    .end((err,res) =>{
    //  res.body.should.be.a('array');
      res.should.have.status(200);
    //  console.log(res.body);
      done();
    });

  });

  it('Return an array ', (done) => {
    chai.request('http://localhost:3000')
    .get('/colapi/v1/users')
    .end((err,res) =>{
      res.body.should.be.a('array');
    //  res.should.have.status(200);
  //    console.log(res.body);
      done();
    });

  });


  it('Return non empty array ', (done) => {
    chai.request('http://localhost:3000')
    .get('/colapi/v1/users')
    .end((err,res) =>{
      res.body.length.should.be.gte(1)
    //  res.should.have.status(200);
  //    console.log(res.body);
      done();
    });

  });

  it('Object user has first_name property ', (done) => {
    chai.request('http://localhost:3000')
    .get('/colapi/v1/users')
    .end((err,res) =>{
      res.body[0].should.have.property('first_name')
    //  res.should.have.status(200);
  //    console.log(res.body);
      done();
    });

  });

  it('Create user ', (done) => {
    chai.request('http://localhost:3000')
    .post('/colapi/v1/users')
    .send({  "first_name": "Test user F",
   "last_name": "from test",
   "email": "test@devhub.com",
   "user": "test1",
   "password": "password",
   "country": "testcountry"
})
    .end((err,res,body) =>{

    //  res.should.have.status(200);
     console.log(body);
     console.log(res.body)
     console.log(res)
      done();
    });

  });

});
